'''
Unit tests for calc
'''

import math
import unittest

from calc import operations


class TestOperations(unittest.TestCase):
    '''
    Test Calc
    '''

    def test_add(self):
        '''
        Test add
        '''
        # ARRANGE
        expected = 4.2

        # ACT
        actual = operations.add(1.3, 2.9)

        # ASSERT
        self.assertTrue(math.isclose(actual, expected))

    def test_subtract(self):
        '''
        Test subtract
        '''
        # ARRANGE
        expected = 2.9

        # ACT
        actual = operations.subtract(5.2, 2.3)

        # ASSERT
        self.assertTrue(math.isclose(actual, expected))

    def test_multiply(self):
        '''
        Test multiply
        '''
        # ARRANGE
        expected = 6.82

        # ACT
        actual = operations.multiply(2.2, 3.1)

        # ASSERT
        self.assertTrue(math.isclose(actual, expected))

    def test_divide(self):
        '''
        Test divide
        '''
        # ARRANGE
        expected = 2.15

        # ACT
        actual = operations.divide(4.3, 2.0)

        # ASSERT
        self.assertTrue(math.isclose(actual, expected))
