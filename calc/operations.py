'''
The calculator's basic operations
'''

def add(augend: float, addend:float) -> float:
    '''
    Performs: augend + addend = sum
    '''
    return augend + addend

def subtract(minuend: float, subtrahend:float) -> float:
    '''
    Performs: minuend - subtrahend = difference
    '''
    return minuend - subtrahend

def multiply(multiplicand: float, multiplier:float) -> float:
    '''
    Performs: multiplicand * multiplier = product
    '''
    return multiplicand * multiplier

def divide(dividend: float, divisor:float) -> float:
    '''
    Performs: dividend / divisor = quotient
    '''
    return dividend / divisor
