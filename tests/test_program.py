'''
Unit tests for calc
'''

import unittest
from unittest.mock import patch

from calc import program


class TestProgram(unittest.TestCase):
    '''
    Test Calc Program
    '''

    @patch('builtins.print')
    def test_run(self, mock_print):
        '''
        Test run function
        '''
        # ARRANGE
        expected = 'Hello calc!'

        # ACT
        program.run()

        # ASSERT
        mock_print.assert_called_with(expected)
