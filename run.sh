#!/bin/bash
pyfiglet Lint
pylint ./calc

pyfiglet Coverage
coverage run --source=calc -m unittest discover
coverage report --fail-under=100
coverage html
