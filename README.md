# python-blueprint

Blueprint for setting up a python project with lint, unit-test and code-coverage.

## Getting Started

1. Create a virtual environment   
   ```python3 -m venv .venv```
1. Activate it   
   ```. .venv/bin/activate```
1. Update pip if a newer version exist   
   ```python3 -m pip install --upgrade pip```
1. Install dependencies   
   ```python3 -m pip install -r requirements.txt```
1. Run lint and unittest   
   ```./run.sh```
